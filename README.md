# PVC-Projeto2

Trabalho 2 da matéria de Principios da visão computacional 2019/1
Feito por Thiago Chaves Monteiro e Larissa Santana de Freitas Andrade
Link do repositório: https://gitlab.com/Suniaster/pvc-projeto2

## Requisitos

Para o uso do progama é necessário que a instalação do software python na versão 3.5.2 tenha sido feito na maquina, assim como da biblioteca opencv, versão 3.3.1, e uma webcam conectada ao computador.

## Especificações

O programa é divido em 3 módulos. O primeiro módulo é independente dos demais e o terceiro depende da execução do segundo.

O módulo 1 abre uma imagem onde é possível selecionar pixels. Quando há a seleção de 2 pixels, é desenhada uma reta entre eles e escrito no terminal a respectiva distância euclidiana. Caso haja a necessidade de mudar essa imagem, é necessário botar a imagem na pasta data e em seguida ir na pasta *src* e mudar, no arquivo *req1.py*  a linha:

```python
img = cv2.imread("data/image.jpg",-1)
```

Para:

```python
img = cv2.imread("data/*nome_da_sua_imagem*", -1)
```

Já para os módulos 2 e 3, é necessário que uma ordem seja obedecida. O módulo 2 abre a webcam do computador e procura frame por frame pelo padrão de calibração em formato de tabuleiro de xadrez, e após acha-lo faz os cálculos da calibração e salva-os em arquivos xml. Já o módulo 3 utiliza-se do resultado calculado anteriormente para estimar tamanhos de objetos que estejam no mesmo plano XY do padrão de calibração encontrado na camêra. Como o módulo 3 depende diretamente dos resultados do segundo módulo, deve ser executado primeiramente o módulo 2. 

No arquivo helper.py é definida a classe responsável por realizar as operações sobre as imagens ou videos e armazenar o conteúdo deles. Já os arquivos req1.py, req2.py e req3.py são apenas scripts para abrir os arquivos desejados e utilizam a lógica criada na classe ImageController para realizar seus processos. Cada um desses arquivos anteriormente citados cumpre uma das requisições do trabalho.

No arquivo calculateMediaIntrinsics.py, são declaradas funções auxiliares para ler os arquivos xml e calcular os valores necessários.

## Utilização

Para usar qualquer um dos módulos, basta rodar o arquivo start.sh passando como parametro a string r*n*, substituindo *n* pelo número do módulo desejado.
Exemplo para rodar o módulo 1:
(Lembrando que para a utilização do módulo 3 é necessário que o módulo 2 seja executado a priori)

```bash
./start.sh r1
```

Exemplo para rodar o módulo 3:

```bash
./start.sh r3
```