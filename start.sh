# !/bin/bash

if [ -z $1 ];
    then
    echo 'Insira qual módulo deve ser executado: '
    echo 'ex: $./start.sh r3'
elif [ $1 = 'r1' ];
    then
    python3 src/req1.py
elif [ $1 = 'r2' ];
    then
    python3 src/req2.py
elif [ $1 = 'r3' ];
    then
    python3 src/req3.py
else
    echo 'Insira parametros válidos...'
    echo '########## Exemplo ###########'
    echo 'Para rodar o 1 requisito digite:'
    echo '$./start.sh r1'
fi