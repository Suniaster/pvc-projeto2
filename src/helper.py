import numpy as np
import cv2
import math

class ImageController:
    def __init__(self, name, img=np.zeros(1), Plinv = None):
        # Parando execução caso haja parametros invalidos
        if type(img) == type(None):
            raise Exception("Imagem inexistente passada de argumento para ImageController.\n Cheque o nome, a extensão e o path da imagem.")

        self.Plinv =Plinv
        self.basePx = (-1,-1,-1)
        self.mouseCrd = {
            "x": -1,
            "y": -1
        }
        # Image Variables
        self.name = name
        self.img = img

        # Colocando valores padrão
        self.isGray = False
        self.hasSelected = False
        self.lineCoordinates = []

        # Mudando valores padrão caso necessário
        if len(img.shape)>1 :
            if len(img.shape) == 2:
                self.isGray = True
            else:
                # No caso de imagens com canal alfa
                if img.shape[2] == 4:
                    pass

    def attMousePos(self,img,x,y):
        """Salva cordenadas do mouse no dicionario e printa no terminal informações sobre o local"""
        self.mouseCrd = {
            "x": x,
            "y": y
        }
        if self.isGray:
            print('X:'+str(x)+', Y:'+str(y)+', Luminosidade: '+str(img[y,x]))
        else:
            print('X:'+str(x)+', Y:'+str(y)+', RGB:'+str(img.item(y,x,2))+' '+str(img.item(y,x,1))+' '+str(img.item(y,x,0)))

    def changeBasePix(self, img, x,y):
        """Muda o pixel base de comparação"""
        self.basePx = img[y,x].copy()


    def MCB_PrintMouse(self,event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.attMousePos(self.img,x,y)

    def attImage(self, img):
        """Atualiza a tela em que a imagem está sendo mostrada"""
        cv2.imshow(self.name, img)

    def drawLine(self, coordinates, img):
        newimg =img.copy()
        cv2.line(newimg, coordinates[0], coordinates[1], (255,126,0), thickness=2, lineType=8)
        return newimg

    def MCB_DrawLine(self,event,x,y,flags,param):
        self.MCB_PrintMouse(event,x,y,flags,param)
        if event == cv2.EVENT_LBUTTONDOWN:
            # Salvando duas novas coordenadas
            if(len(self.lineCoordinates) < 2):
                self.lineCoordinates.append((x,y))
            # Quando salvar a segunda coordenada, cria uma nova imagem com uma linha
            # calcula a distancia dos pontos e mostra os resultados
            if(len(self.lineCoordinates) == 2):
                newimg = self.drawLine(self.lineCoordinates, self.img)
                distance = math.sqrt(((self.lineCoordinates[0][0]-self.lineCoordinates[1][0])**2) + ((self.lineCoordinates[0][1])- self.lineCoordinates[1][1])**2)
                print('Distance: '+str(distance)+'\n')
                self.attImage(newimg)
                self.lineCoordinates = []

    def MCB_SUPERDrawnLine(self,event,x,y,flags,param):
        self.MCB_PrintMouse(event,x,y,flags,param)
        if event == cv2.EVENT_LBUTTONDOWN:
            # Salvando duas novas coordenadas
            if(len(self.lineCoordinates) < 2):
                self.lineCoordinates.append((x,y))
            # Quando salvar a segunda coordenada, cria uma nova imagem com uma linha
            # calcula a distancia dos pontos e mostra os resultados
            if(len(self.lineCoordinates) == 2):
                newimg = self.drawLine(self.lineCoordinates, self.img)
                distance = math.sqrt(((self.lineCoordinates[0][0]-self.lineCoordinates[1][0])**2) + ((self.lineCoordinates[0][1])- self.lineCoordinates[1][1])**2)
                
                self.attImage(newimg)

                appended1 = [self.lineCoordinates[0][0], self.lineCoordinates[0][1], 1]
                appended2 = [self.lineCoordinates[1][0], self.lineCoordinates[1][1], 1]
                worlcord1 = self.calculateWorldCord(appended1,self.Plinv)
                worlcord2 = self.calculateWorldCord(appended2,self.Plinv)

                dist = self.WorldDist(worlcord1, worlcord2)
                print('TAMANHO DA LINHA (cm): '+str(dist))

                self.lineCoordinates = []
    def WorldDist(self,p1,p2):
        return np.linalg.norm(p1-p2)

    def calculateWorldCord(self,pointVector, Plinv):
        worldCord = np.matmul(Plinv,pointVector)
        for i in range(3):
            worldCord[i] /= worldCord[2]
        return worldCord
