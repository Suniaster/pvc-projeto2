import numpy as np
import cv2
import glob

from helper import ImageController
# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
#SQUARE Size em cm
SQUARE_SIZE = 2.80

objp = np.zeros((6*8,3), np.float32)
objp[:,:2] = np.mgrid[0:8,0:6].T.reshape(-1,2)
objp *= SQUARE_SIZE


# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

cap = cv2.VideoCapture(0)
frameInterval = 100
frameCounter = 0
imagenumber = 0

quant_snapshots = 5

quant_snapshots = input("Numero de Snapshots: ")
quant_snapshots = int(quant_snapshots)

while(cap.isOpened()):
    ret, img = cap.read()
    frameCounter +=1 

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imshow('img', img)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (8,6), None)

    # If found, add object points, image points (after refining them)
    if ret == True and frameCounter > frameInterval:
        
        objpoints = [] # 3d point in real world space
        imgpoints = [] # 2d points in image plane.

        objpoints = [objp]
        corners2=cv2.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
        imgpoints = [corners]

        # Draw and display the corners
        cv2.drawChessboardCorners(img, (8,6), corners2, ret)

        ########################### Lendo arquivos ######################################

        ### Pegando matriz de intrinseco calculado:
        file =  cv2.FileStorage("params/intrinsicsMean.xml", cv2.FILE_STORAGE_READ)
        mtxC = file.getNode("intrinsics")
        mtxC = mtxC.mat()
        file.release()

        ### Pegando matriz de distortion
        file =  cv2.FileStorage("params/distortionsMean.xml", cv2.FILE_STORAGE_READ)
        distC = file.getNode("distortions")
        distC = distC.mat()
        file.release()

        ########################### Calibrando extrinsecos ######################################
        mtx=mtxC
        dist=distC
        ret, rvecs, tvecs= cv2.solvePnP(objp, corners2, mtxC, distC)
        
        frameCounter = 0
        rmtx , _= cv2.Rodrigues(rvecs)

        # Matriz de rotação concatenada com a de translação
        # K é a matriz de extrinsecos
        K = np.concatenate((rmtx,tvecs), axis=1)
        
        # Fazendo multiplicação de matriz de intrinsecos e extrinsecos
        P =  np.matmul(mtx, K)

        # Deletando linha relacionada ao eixo Z
        Pl = np.delete(P, 2, axis=1)

        # Invertendo a matriz
        Plinv = np.linalg.inv(Pl)
        
        # Criando objeto para cuidar de operações na imagem
        control = ImageController('img', img, Plinv = Plinv)
        cv2.imshow(control.name, control.img)
        cv2.setMouseCallback(control.name, control.MCB_SUPERDrawnLine)
        # Distancia da origem do tabuleiro até a camera
        norm = np.linalg.norm(tvecs)
        print("DISTANCIA DA CAMERA A ORIGEM: ", norm)
        print("---------------------------------------------------")

        ret, mtx, dist, tvecs, rvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], mtxC, distC)

        h,  w = img.shape[:2]
        newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
        # undistort
        dst = cv2.undistort(img, mtx, dist, None, newcameramtx)

        ret, rvecs, tvecs= cv2.solvePnP(objp, corners2, mtx, dist)
        
        frameCounter = 0
        rmtx , _= cv2.Rodrigues(rvecs)

        # Matriz de rotação concatenada com a de translação
        # K é a matriz de extrinsecos
        K = np.concatenate((rmtx,tvecs), axis=1)


        # Caluclos do P para matriz distorcida
        Pd = np.matmul(newcameramtx, K)
        Pdl = np.delete(Pd,2,axis=1)
        Pdlinv=np.linalg.inv(Pdl)
        control2 = ImageController('undist', dst, Plinv = Pdlinv)
        cv2.imshow(control2.name, control2.img)
        cv2.setMouseCallback(control2.name, control2.MCB_SUPERDrawnLine)


        cv2.waitKey(0)


        imagenumber+=1
        if(imagenumber == quant_snapshots):
            break


    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()