import numpy as np
import cv2
import math  
from helper import ImageController

# Pegando imagem
img = cv2.imread("data/image.jpg",-1)

control = ImageController('image',img)

cv2.imshow(control.name, control.img)

# Função de callback para desenhar linha sem alterar imagem atual
cv2.setMouseCallback(control.name, control.MCB_DrawLine)


cv2.waitKey(0)

cv2.destroyAllWindows()