import cv2
import glob
import numpy as np
import math
import glob
def DoMean(wantedParam):
    
    if wantedParam not in ["intrinsics", "distortions"]:
        print("PASSA DIREITO")
        return

    if wantedParam == "intrinsics":
        mean = np.zeros((3,3))
    elif wantedParam == "distortions":
        mean = np.zeros((1,5))
    
    imgList=glob.glob(wantedParam+'/*.xml')
    quant = len(imgList)

    for imagenumber in range(quant):
        file =  cv2.FileStorage(wantedParam+"/output"+str(imagenumber)+".xml", cv2.FILE_STORAGE_READ)
        fn = file.getNode(wantedParam)
        mean += fn.mat()
        file.release()

    mean /= quant
    newFile = cv2.FileStorage("params/"+wantedParam+"Mean.xml", cv2.FILE_STORAGE_WRITE)
    newFile.write(wantedParam, mean)
    return mean


def DoSTD(wantedParam):
    mean = DoMean(wantedParam)
    
    if wantedParam not in ["intrinsics", "distortions"]:
        print("PASSA DIREITO")
        return
    if wantedParam == "intrinsics":
        std = np.zeros((3,3))
    elif wantedParam == "distortions":
        std = np.zeros((1,5))
    imgList=glob.glob(wantedParam+'/*.xml')
    quant =  len(imgList)

    for imagenumber in range(quant):
        file =  cv2.FileStorage(wantedParam+"/output"+str(imagenumber)+".xml", cv2.FILE_STORAGE_READ)
        fn = file.getNode(wantedParam)
        fn = fn.mat()
        std += (fn - mean)**2
        file.release()
    std/=quant
    std=np.sqrt(std)

    newFile = cv2.FileStorage("params/"+wantedParam+"Deviation.xml", cv2.FILE_STORAGE_WRITE)
    newFile.write(wantedParam, std)

    return (mean, std)

