import numpy as np
import cv2
import glob
from calculateMediaIntrinsecs import *
from helper import ImageController
# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((6*8,3), np.float32)
objp[:,:2] = np.mgrid[0:8,0:6].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

cap = cv2.VideoCapture(0)
frameInterval = 100
frameCounter = 0
imagenumber = 0

quant_snapshots = 5

quant_snapshots = input("Numero de Snapshots: ")
quant_snapshots = int(quant_snapshots)

while(cap.isOpened()):
    ret, img = cap.read()
    frameCounter +=1 

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imshow('img', img)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (8,6), None)

    # If found, add object points, image points (after refining them)
    if ret == True and frameCounter > frameInterval:
        control = ImageController('img', img)

        cv2.setMouseCallback(control.name, control.MCB_DrawLine)

        objpoints.append(objp)
        corners2=cv2.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners)

        # Draw and display the corners
        cv2.drawChessboardCorners(img, (8,6), corners2, ret)
        cv2.imshow('img', img)
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
        frameCounter = 0

        imgUndist = img.copy()
        h,  w = img.shape[:2]
        newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))

        # undistort
        mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)
        dst = cv2.undistort(img, mtx, dist, None, newcameramtx)


        control2 = ImageController('undist', dst)
        cv2.imshow('undist', dst)
        cv2.setMouseCallback(control2.name, control2.MCB_DrawLine)


        file =  cv2.FileStorage("intrinsics/output"+str(imagenumber)+".xml", cv2.FILE_STORAGE_WRITE)
        file.write("intrinsics", mtx)
        file.release()

        file =  cv2.FileStorage("distortions/output"+str(imagenumber)+".xml", cv2.FILE_STORAGE_WRITE)
        file.write("distortions", dist)
        file.release()

        cv2.waitKey(0)
        cv2.destroyWindow('undist')

        imagenumber+=1
        if(imagenumber == quant_snapshots):
            break


    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


DoSTD("intrinsics")
DoSTD("distortions")

cv2.destroyAllWindows()